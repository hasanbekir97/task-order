<?php

namespace Database\Seeders;

use App\Models\Type;
use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Type::truncate();

        // And now, let's create a few types in our database:
        Type::create([
            'name' => "invoice_ops"
        ]);
        Type::create([
            'name' => "custom_ops"
        ]);
        Type::create([
            'name' => "common_ops"
        ]);
    }
}
