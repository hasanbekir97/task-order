<?php

namespace Database\Seeders;

use App\Models\Currency;
use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        Currency::truncate();

        // And now, let's create a few types in our database:
        Currency::create([
            'sign' => "₺"
        ]);
        Currency::create([
            'sign' => "€"
        ]);
        Currency::create([
            'sign' => "$"
        ]);
        Currency::create([
            'sign' => "£"
        ]);
    }
}
