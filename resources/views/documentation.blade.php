<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Task Order System | API Documentation</title>
    <meta name="description" content="Task Order System | API Documentation">
    <meta name="keywords" content='Task Order System | API Documentation'>

    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="lang" content="en" />
    <meta name="author" content="Hasan Bekir DOĞAN" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="robots" content="noindex, follow" />
    <meta name="csrf-token" content="{{ csrf_token() }}">



    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/layout.css')}}">

</head>
<body class="antialiased">

<div style="margin:5% 15%">


    <h1 style="text-align: center">Documentation</h1>

    <hr>

    <h3>Introduction</h3>
    <p>
        Welcome to the Task Order API! This documentation should help you familiarise yourself with the
        resources available and how to consume them with HTTP requests. If you're after a native helper library
        then I suggest you scroll down and check out what's available. Read through the getting started section
        before you dive in. Most of your problems should be solved just by reading through it.
    </p>

    <hr>

    <h3>Base URL</h3>
    <p>The Base URL is the root URL for all of the API, if you ever make a request to api and you get back a 404 NOT FOUND response then check the Base URL first.</p>
    <p>The Base URL for api is:</p>
    <pre><code>https://task-order-system.herokuapp.com/api</code></pre>
    <p>The documentation below assumes you are prepending the Base URL to the endpoints in order to make requests.</p>

    <br>
    <br>
    <br>
    <br>
    <br>

    <h3>Create an Task</h3>
    <p><b class="blue">POST</b> /tasks</p>
    <p>You can create a new task.</p>
    <h5>Headers</h5>
    <pre><code>Content-Type: application/json</code></pre>
    <h5>Request Body (application/json) <i>- example</i></h5>
    <pre><code>{
    "title": "Task 15",
    "type": "custom_ops",
    "country": "TR"
}</code></pre>
    <table style="width: 100%;">
        <tr>
            <td>title:</td>
            <td colspan="2">required</td>
        </tr>
        <tr>
            <td>type:</td>
            <td colspan="2">required</td>
            <td><i>common_ops or custom_ops or invoice_ops can be written to type</i></td>
        </tr>
        <tr>
            <td>country:</td>
            <td colspan="2">if type is custom_ops, then it is required</td>
            <td><i>have to be iso 2 code</i></td>
        </tr>
        <tr>
            <td>currency:</td>
            <td colspan="2">if type is invoice_ops, then it is required</td>
            <td><i>have to be ₺ or € or $ or £</i></td>
        </tr>
        <tr>
            <td>quantity:</td>
            <td colspan="2">if type is invoice_ops, then it is required</td>
            <td><i>have to be integer</i></td>
        </tr>
    </table>
    <br>
    <h5>Response (application/json) <i>- example</i></h5>
    <pre><code>{
    "message": "Task successfully created",
    "task": {
        "id": 12,
        "title": "Task 15",
        "type": "custom_ops",
        "country": "TR"
    }
}</code></pre>

    <hr>

    <h3>Get Tasks</h3>
    <p><b class="green">GET</b> /tasks</p>
    <p>You can get tasks that recorded on the system.</p>
    <h5>Headers</h5>
    <pre><code>Content-Type: application/json</code></pre>
    <h5>Response (application/json) <i>- example</i></h5>
    <pre><code>[
    {
        "id": 5,
        "title": "Task 1",
        "type": "common_ops",
        "prerequisites": [
            6,
            9,
            10,
            11
        ]
    },
    {
        "id": 6,
        "title": "Task 2",
        "type": "common_ops",
        "prerequisites": []
    },
    {
        "id": 7,
        "title": "Task 3",
        "type": "common_ops",
        "prerequisites": [
            11,
            8,
            6,
            9
        ]
    },
    {
        "id": 8,
        "title": "Task 4",
        "type": "invoice_ops",
        "amount": {
            "currency": "€",
            "quantity": "5000.00"
        },
        "prerequisites": []
    },
    {
        "id": 9,
        "title": "Task 5",
        "type": "invoice_ops",
        "amount": {
            "currency": "$",
            "quantity": "8000.00"
        },
        "prerequisites": [
            11
        ]
    },
    {
        "id": 10,
        "title": "Task 6",
        "type": "custom_ops",
        "country": "TR",
        "prerequisites": [
            7,
            8
        ]
    },
    {
        "id": 11,
        "title": "Task 7",
        "type": "custom_ops",
        "country": "DE",
        "prerequisites": []
    },
    {
        "id": 12,
        "title": "Task 15",
        "type": "custom_ops",
        "country": "TR",
        "prerequisites": []
    }
]</code></pre>

    <hr>

    <h3>Add Prerequisites to a Task</h3>
    <p><b class="blue">POST</b> /tasks/{task_id}/prerequisites</p>
    <p>You can create a new task.</p>
    <h5>Path Parameters</h5>
    <pre><code>task_id: integer</code></pre>
    <h5>Headers</h5>
    <pre><code>Content-Type: application/json</code></pre>
    <h5>Request Body (application/json) <i>- example</i></h5>
    <pre><code>{
    "prerequisities": [
        7,
        8
    ]
}</code></pre>
    <table style="min-width:700px;">
        <tr>
            <td>prerequisities:</td>
            <td colspan="2">required</td>
            <td><i>have to be integer array</i></td>
        </tr>
    </table>
    <br>
    <h5>Response (application/json) <i>- example</i></h5>
    <pre><code>{
    "message": "Prerequisites have been successfully added",
    "task_id": "10",
    "prerequisities": [
        7,
        8
    ],
    "data_already_registered": []
}</code></pre>

    <hr>

    <h3>Order Tasks</h3>
    <p><b class="green">GET</b> /tasks/order</p>
    <p>You can order tasks according to prerequisites.</p>
    <h5>Headers</h5>
    <pre><code>Content-Type: application/json</code></pre>
    <h5>Response (application/json) <i>- example</i></h5>
    <pre><code>{
    "message": "Tasks have been successfully ordered",
    "order_list": [
        {
            "id": 6,
            "title": "Task 2"
        },
        {
            "id": 11,
            "title": "Task 7"
        },
        {
            "id": 8,
            "title": "Task 4"
        },
        {
            "id": 9,
            "title": "Task 5"
        },
        {
            "id": 7,
            "title": "Task 3"
        },
        {
            "id": 10,
            "title": "Task 6"
        },
        {
            "id": 5,
            "title": "Task 1"
        }
    ],
    "unordered_list": [
        {
            "id": 12,
            "title": "Task 15"
        }
    ]
}</code></pre>
    <p class="red">*** order_list shows order list that have prerequisites. But, unordered_list shows unordered list that haven't any prerequisites. So, tasks that are in unordered list can be done any time.</p>


</div>


    <script type="text/javascript" src="{{asset('assets/js/bootstrap.min.js')}}"></script>

</body>
</html>
