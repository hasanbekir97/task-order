
**Project Name:** Task Order

**Project Description:** A system where user can create, show task and order these tasks according to prerequisites on API system.

##

**Users can do these functions:**

- Task
    - create tasks
    - list tasks
    - add prerequisites to a task
    - order the tasks according to prerequisites

##

    You will seed the database using the seeder classes after run migrations.

##

### Documentation

API documentation is located at [https://task-order-system.herokuapp.com](https://task-order-system.herokuapp.com)

##

**Technologies that used in the project.**

- Object Oriented Programming
- PHP
- Laravel Framework
- Laravel Repository Pattern
- JSON
- SQL
- PostgreSQL
- REST API
