<?php

namespace App\Http\Controllers;

use App\Interfaces\TaskRepositoryInterface;
use App\Models\Currency;
use App\Models\Prerequisite;
use App\Models\Task;
use App\Models\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{
    private TaskRepositoryInterface $taskRepository;

    public function __construct(TaskRepositoryInterface $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    public function index(){
        return response()->json($this->taskRepository->getAllTasks(),200);
    }

    public function create(Request $request){
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|between:1,255',
            'type' => 'required|string|between:1,255',
            'country' => 'string|between:1,255',
            'currency' => 'string|between:1,255',
            'quantity' => 'numeric'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        if($this->checkValidType($request->type) === false){
            return response()->json([
                'message' => 'Invalid Type'
            ],404);
        }

        $checkExtraTypeData = $this->checkExtraType($request->all());

        if(count($checkExtraTypeData) !== 0){
            return response()->json($checkExtraTypeData,200);
        }

        $data = Type::where('name', $request->type)->get();
        $type_id = $data[0]->id;

        $taskDetails = [
            'title' => $request->title,
            'type_id' => $type_id
        ];

        $taskResponse = [
            'title' => $request->title,
            'type' => $request->type
        ];

        if($type_id === 2){
            $taskDetails['country'] = strtoupper($request->country);

            $taskResponse['country'] = strtoupper($request->country);
        }
        else if($type_id === 1){
            $data = Currency::where('sign', $request->currency)->get();

            $taskDetails['currency_id'] = $data[0]->id;
            $taskDetails['quantity'] = $request->quantity;

            $taskResponse['amount'] = [
                'currency' => $data[0]->sign,
                'quantity' => $request->quantity
            ];
        }

        $task = $this->taskRepository->createTask($taskDetails);

        return response()->json([
            'message' => 'Task successfully created',
            'task' => array_merge([
                'id' => $task->id
            ], $taskResponse)
        ],201);
    }

    public function addPrerequisites(Request $request, $id){

        $checkValidTaskIdsData = $this->checkValidTaskIds($request->all());

        if(count($checkValidTaskIdsData) !== 0){
            return response()->json([
                'message' => 'There are some invalid task ids',
                'Invalid Ids' => $checkValidTaskIdsData
            ],200);
        }

        return response()->json($this->taskRepository->addPrerequisites($request->prerequisities, $id), 201);
    }

    public function order(){
        $data = Prerequisite::all();

        $nodeIds = [];
        $edges = [];

        foreach($data as $row){
            if(in_array($row->prerequisite_task_id, $nodeIds) === false)
                array_push($nodeIds, $row->prerequisite_task_id);
            if(in_array($row->task_id, $nodeIds) === false)
                array_push($nodeIds, $row->task_id);

            array_push($edges, [$row->prerequisite_task_id, $row->task_id]);
        }

        $taskOrderList = $this->topologicalSort($nodeIds, $edges);

        return response()->json(
            $this->taskRepository->orderTasks($taskOrderList)
        ,200);
    }

    protected function checkValidType($type){
        $data = Type::where('name', $type)->get();

        if(count($data) !== 0)
            $message = true;
        else
            $message = false;

        return $message;
    }

    protected function checkExtraType($response){
        $type = Type::where('name', $response['type'])->get();

        $messageData = [];

        // this is for invoice_ops
        if($type[0]->id === 1){
            if(empty($response['currency']))
                array_push($messageData, "required currency");
            else if($this->checkValidCurrency($response['currency']) === false)
                array_push($messageData, "invalid currency");
            else if(empty($response['quantity']))
                array_push($messageData, "required quantity");

        }

        // this is for custom_ops
        if($type[0]->id === 2){
            if(empty($response['country']))
                array_push($messageData, "required country");
            else if($this->checkValidCountry($response['country']) === false)
                array_push($messageData, "invalid country");

        }

        return $messageData;
    }

    protected function checkValidCurrency($currency){
        $data = Currency::where('sign', $currency)->get();

        if(count($data) !== 0)
            $message = true;
        else
            $message = false;

        return $message;
    }

    protected function checkValidCountry($country){
        // checks that the country is valid or not.
        $responseValidateCountry = Http::get("http://api.worldbank.org/v2/country/$country?format=json");

        $dataValidateCountry = json_decode($responseValidateCountry->body());

        if(isset($dataValidateCountry[0]->message) && $dataValidateCountry[0]->message[0]->key === "Invalid value"){
            $message = false;
        }
        else
            $message = true;

        return $message;
    }

    protected function checkValidTaskIds($response){
        $data = $response['prerequisities'];
        $invalidList = [];
        foreach($data as $row){
            if(gettype($row) !== "integer")
                array_push($invalidList, $row);
            else{
                $checkId = Task::where('id', $row)->get();
                if(count($checkId) === 0)
                    array_push($invalidList, $row);
            }
        }

        return $invalidList;
    }

    protected function topologicalSort($nodeids, $edges) {

        // initialize variables
        $L = $S = $nodes = array();

        // remove duplicate nodes
        $nodeids = array_unique($nodeids);

        // remove duplicate edges
        $hashes = array();
        foreach($edges as $k=>$e) {
            $hash = md5(serialize($e));
            if (in_array($hash, $hashes)) { unset($edges[$k]); }
            else { $hashes[] = $hash; };
        }

        // Build a lookup table of each node's edges
        foreach($nodeids as $id) {
            $nodes[$id] = array('in'=>array(), 'out'=>array());
            foreach($edges as $e) {
                if ($id==$e[0]) { $nodes[$id]['out'][]=$e[1]; }
                if ($id==$e[1]) { $nodes[$id]['in'][]=$e[0]; }
            }
        }

        // While we have nodes left, we pick a node with no inbound edges,
        // remove it and its edges from the graph, and add it to the end
        // of the sorted list.
        foreach ($nodes as $id=>$n) { if (empty($n['in'])) $S[]=$id; }
        while (!empty($S)) {
            $L[] = $id = array_shift($S);
            foreach($nodes[$id]['out'] as $m) {
                $nodes[$m]['in'] = array_diff($nodes[$m]['in'], array($id));
                if (empty($nodes[$m]['in'])) { $S[] = $m; }
            }
            $nodes[$id]['out'] = array();
        }

        // Check if we have any edges left unprocessed
        foreach($nodes as $n) {
            if (!empty($n['in']) or !empty($n['out'])) {
                return null; // not sortable as graph is cyclic
            }
        }
        return $L;
    }

}
