<?php

namespace App\Repositories;

use App\Interfaces\TaskRepositoryInterface;
use App\Models\Currency;
use App\Models\Prerequisite;
use App\Models\Task;
use App\Models\Type;
use Illuminate\Support\Facades\Auth;

class TaskRepository implements TaskRepositoryInterface
{
    public function getAllTasks(){
        $tasks = Task::all();
        $prerequisites = Prerequisite::all();

        $responseArray = [];

        foreach($tasks as $task){
            $prerequisitesArray = [];
            $type = Type::where('id', $task->type_id)->get();

            foreach($prerequisites as $prerequisite){
                if($prerequisite->task_id === $task->id) {
                    array_push($prerequisitesArray,
                        $prerequisite->prerequisite_task_id
                    );
                }
            }

            $tempArray = [
                'id' => $task->id,
                'title' => $task->title,
                'type' => $type[0]->name,
            ];

            if($task->type_id === 2){
                $tempArray['country'] = $task->country;
            }

            if($task->type_id === 1){
                $currency = Currency::where('id', $task->currency_id)->get();
                $tempArray['amount'] = [
                    'currency' => $currency[0]->sign,
                    'quantity' => $task->quantity
                ];
            }

            $tempArray['prerequisites'] = $prerequisitesArray;

            array_push($responseArray, $tempArray);
        }

        return $responseArray;
    }
    public function createTask(array $TaskDetails){
        $task = new Task();
        $task->title = $TaskDetails['title'];
        $task->type_id = $TaskDetails['type_id'];
        if(isset($TaskDetails['currency_id']))
            $task->currency_id = $TaskDetails['currency_id'];
        if(isset($TaskDetails['quantity']))
            $task->quantity = $TaskDetails['quantity'];
        if(isset($TaskDetails['country']))
            $task->country = $TaskDetails['country'];
        $task->save();

        return $task;
    }
    public function addPrerequisites(array $prerequisitesDetails, $taskId){
        $failedDataList = [];
        $successfulDataList = [];
        $data = Prerequisite::where('task_id', $taskId)->get();

        foreach($data as $row){
            if(in_array($row->prerequisite_task_id, $prerequisitesDetails)) {
                array_push($failedDataList, $row->prerequisite_task_id);
            }
        }

        foreach ($prerequisitesDetails as $row){
            if(in_array($row, $failedDataList)) {
                continue;
            }
            array_push($successfulDataList, $row);

            $prerequisites = new Prerequisite();
            $prerequisites->task_id = $taskId;
            $prerequisites->prerequisite_task_id = $row;
            $prerequisites->save();
        }

        return [
            "message" => "Prerequisites have been successfully added",
            "task_id" => $taskId,
            "prerequisities" => $successfulDataList,
            "data_already_registered" => $failedDataList
        ];
    }
    public function orderTasks(array $taskOrderList){
        $taskOrderResponseList = [];
        $taskUnorderedResponseList = [];

        foreach($taskOrderList as $row){
            $temp = Task::where('id', $row)
                ->get();
            array_push($taskOrderResponseList, [
                'id' => $row,
                'title' => $temp[0]->title
            ]);
        }

        $data = Task::all();

        foreach($data as $row){
            if(in_array($row->id, $taskOrderList) === false) {
                array_push($taskUnorderedResponseList, [
                    'id' => $row->id,
                    'title' => $row->title
                ]);
            }
        }

        return [
            'message' => 'Tasks have been successfully ordered',
            'order_list' => $taskOrderResponseList,
            'unordered_list' => $taskUnorderedResponseList
        ];
    }
}
