<?php

namespace App\Interfaces;

interface TaskRepositoryInterface
{
    public function getAllTasks();
    public function createTask(array $taskDetails);
    public function addPrerequisites(array $prerequisitesDetails, $taskId);
    public function orderTasks(array $taskDetails);
}
