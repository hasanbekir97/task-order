<?php

use App\Http\Controllers\TaskController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('tasks', [TaskController::class, 'index']);
Route::post('tasks', [TaskController::class, 'create']);
Route::post('tasks/{task}/prerequisites', [TaskController::class, 'addPrerequisites']);
Route::get('tasks/order', [TaskController::class, 'order']);
